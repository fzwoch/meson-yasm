# MIT License
#
# Copyright (c) 2017 Florian Zwoch
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

project('meson-yasm')

yasm_sp = subproject('yasm')
if not yasm_sp.get_variable('yasm').found()
	error('YASM is required for this example')
endif

yasm_app = executable('yasm_app',
	'src/main.c',
	yasm_sp.get_variable('yasm_generator').process(
		meson.current_source_dir() + '/src/yasm.asm'
	),
)

msvc_hack = []
if meson.get_compiler('c').get_id() == 'msvc'
	msvc_hack = 'src/main.c'
endif

yasm_shared_lib = shared_library('yasm_shared',
	yasm_sp.get_variable('yasm_generator').process(
		meson.current_source_dir() + '/src/yasm.asm'
	),
	msvc_hack,
	vs_module_defs : 'yasm.def'
)

yasm_shared_app = executable('yasm_shared_app',
	'src/main.c',
	link_with : yasm_shared_lib,
)

yasm_static_lib = static_library('yasm_static',
	yasm_sp.get_variable('yasm_generator').process(
		meson.current_source_dir() + '/src/yasm.asm'
	)
)

yasm_static_app = executable('yasm_static_app',
	'src/main.c',
	link_with : yasm_static_lib,
)

test('Run YASM app', yasm_app, is_parallel : false)
test('Run YASM shared app', yasm_shared_app, is_parallel : false)
test('Run YASM static app', yasm_static_app, is_parallel : false)
